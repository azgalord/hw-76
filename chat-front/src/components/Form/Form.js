import React from 'react';
import Input from "./Input/Input";
import './Form.css';
import Button from "./Button/Button";

const Form = props => {
  return (
    <div className="Form">
      <Input
        name="author"
        value={props.authorText}
        className="InputAuthor"
        onChange={props.change}
        placeholder="Author"
      />
      <Input
        name="message"
        value={props.messageText}
        className="InputMessage"
        onChange={props.change}
        placeholder="Message"
      />
      <Button
        onClick={props.sendMessage}
      />
    </div>
  );
};

export default Form;