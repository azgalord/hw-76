import React from 'react';
import Message from "./Message/Message";
import './Messages.css';

const Messages = props => {
  return (
      <div className="Messages">
        {props.messages.map((message) => (
          <Message
              time={message.datetime}
              author={message.author}
              message={message.message}
              key={message.id}
          />
        ))}
      </div>
)
  ;
};

export default Messages;