import axios from '../axios-messages';

export const GET_SUCCESS = 'GET_SUCCESS';

export const getSuccess = response => ({type: GET_SUCCESS, response});

export const fetchMessages = url => {
  return dispatch => {
    axios.get(url).then(
      response => dispatch(getSuccess(response.data))
    )
  }
};

export const postMessageToAPI = message => {
  return dispatch => {
    axios.post('/messages', message).then(
      response => {},
      error => {},
    )
  }
};