import {GET_SUCCESS} from "./messagesActions";

const initialState = {
  messages: null,
  lastDate: '',
};

const messagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case (GET_SUCCESS):
      return {
        ...state,
        messages: action.response,
        lastDate: action.response[action.response.length - 1].datetime,
      };
    default:
      return state;
  }
};

export default messagesReducer;