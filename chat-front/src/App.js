import React, { Component } from 'react';
import Messages from "./components/Messages/Messages";
import Form from "./components/Form/Form";

import {connect} from 'react-redux';
import {fetchMessages, postMessageToAPI} from './store/messagesActions';

import './App.css';

class App extends Component {
  state = {
    author: '',
    message: '',
  };

  componentDidMount() {
    // this.getLastMessages();

    this.props.getMessages('/messages');
  }

  // componentDidUpdate() {
  //   if (this.props.lastDate) {
  //     this.getLastMessages();
  //   }
  // }

  componentWillUnmount(){
    clearInterval(this.interval);
  }
  interval = () => {
    this.props.getMessages(`/messages?datetime=${this.props.lastDate}`);
  };
  getLastMessages = () => {
    clearInterval(this.interval);
    setInterval(this.interval, 3000);
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name] : event.target.value});
  };

  render() {
    if (!this.props.messages) {
      return <div>Loading...</div>;
    }

    return (
      <div className="App">
        <Messages
          messages={this.props.messages}
        />
        <Form
            authorText={this.state.author}
            change={event => this.inputChangeHandler(event)}
            messageText={this.state.message}
            sendMessage={() => this.props.postMessageToAPI({
              "message": this.state.message,
              "author": this.state.author
            })}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages,
  lastDate: state.lastDate,
});

const mapDispatchToProps = dispatch => ({
  getMessages: url => dispatch(fetchMessages(url)),
  postMessageToAPI: message => dispatch(postMessageToAPI(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
