const express = require('express');
const router = express.Router();
const fs = require('fs');
const fileName = './messages.json';
const nanoid = require('nanoid');

let data = [];

const checkIfDataContents = () => {
  try {
    const fileContent = fs.readFileSync(fileName);
    data = JSON.parse(fileContent);
  } catch (e) {
    data = [];
  }
};

router.get('/', (req, res) => {
  checkIfDataContents();
  console.log(req.query.datetime);

  if (data.length > 30) {
    data.splice(0, data.length - 30);
  }
  res.send(JSON.stringify(data));
});

router.post('/', (req, res) => {
  if (req.body.message && req.body.author) {
    checkIfDataContents();
    const date = new Date().toISOString();

    req.body.id = nanoid();
    req.body.datetime = date;
    data.push(req.body);
    data = data.sort((a, b) => new Date(a.datetime) - new Date(b.datetime));

    fs.writeFileSync(fileName, JSON.stringify(data, null, 2));
    res.send('File saved!');
  } else {
    res.status(400).send({"error": "Author and message must be present in the request"});
  }
});

module.exports = router;